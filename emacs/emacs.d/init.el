(package-initialize)

(setq load-path (append '(
			  "~/.emacs.d/lisp"
			  ) load-path))
(load "general")
(load "style")
