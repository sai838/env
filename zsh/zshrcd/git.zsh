alias g="git"
alias st="git status"
alias br="git branch"
alias co="git checkout"
alias add="git add"

# for gitk setting
export DISPLAY=:0
